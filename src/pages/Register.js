


import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../userContext';
import Swal from 'sweetalert2';

export default function Register() {

  const { user, setUser } = useContext(UserContext);

  // State hooks to store the values of our input fields
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [mobileNumber, setMobileNumber] = useState('');

  const [email, setEmail] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  



  // State to determine whether the submit button is enabled or not
  const [isActive, setIsActive] = useState(false);

  console.log(firstName);
  console.log(lastName);
  console.log(mobileNumber);
  console.log(email);
  console.log(password1);
  console.log(password2);


  // useEffect() - whenever there is a change in our webpage
  useEffect (() => {
    if((firstName !=='' && lastName !== '' && mobileNumber.length === 11 && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)) {

      setIsActive(true)

    } else {

      setIsActive(false)

    }
  })

  function registerUser(e) {

    e.preventDefault();

/*
    // Check if email already exists
    const users = JSON.parse(localStorage.getItem('users') || '[]');
    if (users.find((u) => u.email === email)) {
      alert('Email already exists');
      return;
    }

    // Add user to localStorage
    const newUser = {
      firstName,
      lastName,
      mobileNumber,
      email,
      password: password1,
    };
    users.push(newUser);
    localStorage.setItem('users', JSON.stringify(users));

*/

     // Process a fetch request to the corresponding backend API
      fetch('http://localhost:4000/users/register', {
        method: "POST",
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          firstName: firstName,
          lastName: firstName,
          mobileNumber: mobileNumber,
          email: email,
          password1: password1,
          password2: password2,

        })
      })
      .then(res => res.json())
      .then(data => {

        console.log(data)

        // If no user information is found, the "access" property will not be available and will return undefined
          // Using the typeof operator will return a string of the data type of the variable/expression it preceeds which is why the value being compared is in a string data type
        if (typeof data.access !== "undefined") {

          localStorage.setItem('token', data.access)
          retrieveUserDetails(data.access)

          Swal.fire({
            title: "Registration Successful",
            icon: "success",
            text: "Welcome to Zuitt!"
          })

        } else {

          Swal.fire({
            title: "Registration failed",
            icon: "error",
            text: "Check your details and try again!"
          })

        }

      })



    // clear input fields
    setFirstName('');
    setLastName(''); 
    setMobileNumber(''); 

    setEmail('');
    setPassword1('');
    setPassword2('');



    alert('Thank you for registering!');
  }

  const retrieveUserDetails = (token) => {

      // The token will be sent as part of the request's header information
      // We put "Bearer" in front of the token to follow implementation standards for JWTs
      fetch('http://localhost:4000/users/details', {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      .then(res => res.json())
      .then(data => {

        console.log(data);

        // Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      })

    }



  // onChange - checks if there are any changes inside of the input fields.
  // value={email} - the value stroed in the input field will come from the value inside the getter "email"
  // setEmail(e.target.value) - sets the value of the getter ëmail to the value stored in the value={email} inside the input field
  return (
    (user.email !== null) ?
      <Navigate to="/register" />
    :
    <Form onSubmit={e => registerUser(e)}>
   
        <Form.Group className="mb-3" controlId="formBasicFirstName">
          <Form.Label>First name</Form.Label>
          <Form.Control required type="text" placeholder="First name" value={firstName} onChange={e => setFirstName(e.target.value)}/>
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicLastName">
          <Form.Label>Last name</Form.Label>
          <Form.Control required type="text" placeholder="Last name" value={lastName} onChange={e => setLastName(e.target.value)}/>
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicMobileNumber">
          <Form.Label>Mobile number</Form.Label>
          <Form.Control required type="tel" placeholder="Mobile number" value={mobileNumber} onChange={(e) => setMobileNumber(e.target.value)} pattern="[0-9]{11}"/>
        </Form.Group>


        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
          <Form.Text className="text-muted">
            We'll never share your email with anyone else.
          </Form.Text>
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)}/>
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword2">
          <Form.Label>Verify Password</Form.Label>
          <Form.Control type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)}/>
        </Form.Group>

        {/*Ternary Operator*/}
        {/*
      if (isActive === true) {
        <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
      } else {
        <Button variant="danger" type="submit" id="submitBtn" disabled>Submit</Button>
      }
        */}
        {
          isActive ?
            <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
          :
            <Button variant="danger" type="submit" id="submitBtn" disabled>Submit</Button>
        }
        
      </Form>
  )
}


