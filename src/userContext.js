import React from "react";
//Create a context object
//A contxt object is a data type of an object that can be used to store info that can be shared to other components within the app. 
//The context object is a diff approach to passing info between components and allows easier access by avoiding prop-drilling

const UserContext = React.createContext();

//The "Provider" component allows other components to consume /use the context object and supply the necessary info needed to the context  object
export const UserProvider = UserContext.Provider;

export default UserContext;